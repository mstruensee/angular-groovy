'use strict';



var technologiesController = function () {
  this.technologies = [
    'AngularJS',
    'Bootstrap',
    'HTML 5',
    'Dropwizard',
    'Groovy',
    'Spring',
    'Spock'
  ];
};

angular.module('technologiesModule', [])
  .controller('technologiesController', [technologiesController])
  .config(function ($routeProvider) {
    $routeProvider.when('/technologies', {
      templateUrl: 'components/technologies/technologies.html',
      controller: 'technologiesController',
      controllerAs: 'technologiesAs'
    });
  });




