package com.betaprogrammers.dotcom.services

import spock.lang.Specification
import spock.lang.Subject

class TechnologiesServiceSpec extends Specification{

  @Subject
  def sut = new TechnologiesService()//sut means system under test

  def "technologies by variable reference"() {
    setup:
    def result = ['Groovy', 'Spring', 'Dropwizard', 'Grunt', 'Angular', 'Bower', 'Node'] as Set

    expect:
    sut.technologies == result
  }

}
