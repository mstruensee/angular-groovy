package com.betaprogrammers.dotcom.controllers

import com.betaprogrammers.dotcom.services.TechnologiesService
import spock.lang.Specification
import spock.lang.Subject

class TechnologiesControllerSpec extends Specification{

  @Subject
  def sut = new TechnologiesController(technologiesService: Mock(TechnologiesService))//sut means system under test .. the variable technologiesService object is being mocked in this case ...

  def "technologies from service"(){
    setup:
    def technologies = ['abc', '123'] as Set

    when:
    //2 things are happening here ...
    // 1) [1 * xxxxx] means that this line of code will be executed once and only once during the THEN call
    // 2) [xxx >> technologies] means that when this line of code is executed mock/inject "technologies" as the result of that line of code execution
    1 * sut.technologiesService.technologies >> technologies

    then:
    sut.technologiesUsed() == [technologies: technologies]
  }

}
