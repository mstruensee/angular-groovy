package com.betaprogrammers.dotcom.config


import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.betaprogrammers.dotcom"])
class SpringConfiguration {}
