package com.betaprogrammers.dotcom

import com.betaprogrammers.dotcom.config.SpringConfiguration
import com.yammer.dropwizard.Service
import com.yammer.dropwizard.assets.AssetsBundle
import com.yammer.dropwizard.config.Bootstrap
import com.yammer.dropwizard.config.Configuration
import com.yammer.dropwizard.config.Environment
import groovy.transform.CompileStatic
import lombok.extern.slf4j.Slf4j
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter

import javax.ws.rs.Path

//E:\code\dotcom\build\libs>java -jar E:\code\dotcom\build\libs\dotcom-1.0-all.jar server E:\code\dotcom\build\libs\config.yml

@Slf4j
class Application extends Service<Configuration> {
  @Override
  void initialize(Bootstrap<Configuration> bootstrap) {
    bootstrap.addBundle new AssetsBundle("/ui", "/", "/index.html")
  }

  @Override
  @CompileStatic
  void run(Configuration configuration, Environment environment) throws ClassNotFoundException {
    initializeSpring configuration, environment

    def filter = environment.addFilter CrossOriginFilter.class, "/*"
    filter.setInitParam CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,PUT,DELETE"
    filter.setInitParam CrossOriginFilter.PREFLIGHT_MAX_AGE_PARAM, String.valueOf(60 * 60 * 24)

    def rewritFilter = environment.addFilter UrlRewriteFilter.class, "/*"
    rewritFilter.setInitParam "confPath", "ui/urlrewrite.xml"
  }

  static void main(String... args) throws Exception {
    new Application().run args
  }

  static void initializeSpring(configuration, environment) {
    def parentContext = new AnnotationConfigWebApplicationContext()
    def context = new AnnotationConfigWebApplicationContext()

    parentContext.refresh()
    parentContext.getBeanFactory().registerSingleton "configuration", configuration
    parentContext.registerShutdownHook()
    parentContext.start()
    context.setParent parentContext
    context.register SpringConfiguration.class
    context.refresh()
    context.registerShutdownHook()
    context.start()

    def resources = context.getBeansWithAnnotation Path.class
    resources.entrySet().each {
      environment.addResource it.value
    }
  }
}