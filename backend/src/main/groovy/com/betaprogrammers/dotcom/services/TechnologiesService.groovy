package com.betaprogrammers.dotcom.services

import lombok.extern.slf4j.Slf4j
import org.springframework.stereotype.Service

@Slf4j
@Service
class TechnologiesService {
  def technologies = ['Groovy', 'Spring', 'Dropwizard', 'Grunt', 'Angular', 'Bower', 'Node'] as Set
}
