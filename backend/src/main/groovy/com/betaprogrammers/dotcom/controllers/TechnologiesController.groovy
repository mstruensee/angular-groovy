package com.betaprogrammers.dotcom.controllers

import com.yammer.metrics.annotation.Timed
import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Slf4j
@Controller
@Path('technologies')
@Produces(MediaType.APPLICATION_JSON)
class TechnologiesController {

  @Autowired
  def technologiesService

  @GET
  @Timed
  @Path('used')
  def technologiesUsed() {
    [technologies: technologiesService.technologies]
  }

}